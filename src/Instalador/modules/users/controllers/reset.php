<?php 
_setView(__FILE__);
_setTitle('Hermes');
_setLayout('blank');

if(check_login_bool()) {
	refresh('/'.$languageURL);
}

if (isset($_POST['reset'])){
    require_once ROOT_PATH.'modules/users/models/users.class.php';
    $usersClass = new Users();
	$s = $usersClass->reset_password();
	
	if ($s === true){
		refresh('/'.$languageURL.'users/login/', 'A new password has been emailed to you.', 'complete');
	}
	else{
		refresh('/'.$languageURL.'users/reset/', $s, 'error');
	}
}