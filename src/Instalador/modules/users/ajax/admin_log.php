<?php
date_default_timezone_set('UTC');
error_reporting(E_ALL);
include("../../../data/config/config.php");
$PDO = PDO_CONNECT();

function relative_date($timestamp, $days = false, $format = "M j, Y") {

    if (!is_numeric($timestamp)) {
        // It's not a time stamp, so try to convert it...
        $timestamp = strtotime($timestamp);
    }

    if (!is_numeric($timestamp)) {
        // If its still not numeric, the format is not valid
        return false;
    }

    // Calculate the difference in seconds
    $difference = time() - $timestamp;

    // Check if we only want to calculate based on the day
    if ($days && $difference < (60*60*24)) {
        return "Hoy";
    }
    if ($difference < 3) {
        return "Justo ahora";
    }
    if ($difference < 60) {
        //  return $difference . " seconds ago";
        return "Hace unos segundos";
    }
    if ($difference < (60*2)) {
        return "Hace 1 minuto";
    }
    if ($difference < (60*60)) {
        return "Hace ". intval($difference / 60) . " minutos";
    }
    if ($difference < (60*60*2)) {
        return "Hace 1 hora";
    }
    if ($difference < (60*60*24)) {
        return "Hace ". intval($difference / (60*60)) . " horas";
    }
    if ($difference < (60*60*24*2)) {
        return "Hace 1 día";
    }
    if ($difference < (60*60*24*7)) {
        return "Hace ". intval($difference / (60*60*24)) . " días";
    }
    if ($difference < (60*60*24*7*2)) {
        return "Hace 1 semana";
    }
    if ($difference < (60*60*24*7*(52/12))) {
        return "Hace ". intval($difference / (60*60*24*7)) . " semanas";
    }
    if ($difference < (60*60*24*7*(52/12)*2)) {
        return "1 month ago";
    }
    if ($difference < (60*60*24*364)) {
        return "Hace ". intval($difference / (60*60*24*7*(52/12))) . " meses";
    }

    // More than a year ago, just return the formatted date
    return @date($format, $timestamp);

}

$request_id = $_POST['request_id'];
$command = "SELECT * FROM chats JOIN chat_requests ON chats.request_id = chat_requests.request_id WHERE chats.request_id = :request_id ORDER BY date ASC";

$result = $PDO->prepare($command);
$result->bindParam(':request_id', $request_id);
$result->execute();


while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $prefix = 'a';

    if ($row['operator_name'] == ''){
        $row['operator_name'] = 'Support Team';
    }
    $sentBy = $row['operator_name'];
    if ($row['operator_id'] == 0){
        $prefix = 'u';
        $sentBy = 'Anonymous';
        if ($row['name'] != 'utc_client'){
            $sentBy  = $row['name'];
        }

    }

    $relativeDate = relative_date($row['date']);
	
	$row['content'] = str_replace("&amp;","&", $row['content']);
	$row['content'] = htmlspecialchars_decode($row['content']);
	
    if (strpos($row['content'], '</transfer>') !== false) {
        $output['rows'][] = '<div class="'.$prefix.'_reply"><span style="float: left; margin-top: 40px; width: 100%;">'.$row['content'].'</span></div>';
    }
    else{
        $output['rows'][] = '<div class="'.$prefix.'_reply"><span class="'.$prefix.'_1"></span><span class="'.$prefix.'_2"><b>'.$sentBy.'</b></span><span class="'.$prefix.'_3">'.$relativeDate.'</span><span class="'.$prefix.'_4">'.replaceEmoticons($row['content']).'</span></div>';
    }
	$output['date'] = $row['date'];
    $output['operator_id'][] = $row['operator_id'];
    $output['user_typing'] = $row['u_status'];
}



function replaceEmoticons($text) {
		
	//Emoticons
	$emoji['emoticons'] = array(

		':)' => 'happy.png',
		':(' => 'sad.png',
		':P' => 'tongue.png',
		';)' => 'wink.png',
		':x' => 'angry.png',
	
		':|' => 'expressionless.png',
		':D' => 'laugh.png',
		':S' => 'puzzled.png',
		'8-)' => 'cool.png',
		':O' => 'surprised.png',
	
		':asleep:' => 'asleep.png',
		':bashful:' => 'bashful.png',
		':bashfulcute:' => 'bashfulcute.png',
		':bigevilgrin:' => 'bigevilgrin.png',
		':bigsmile:' => 'bigsmile.png',
	
		':bigwink:' => 'bigwink.png',
		':chuckle:' => 'chuckle.png',
		':crying:' => 'crying.png',
		':confused:' => 'confused.png',
		':confusedsad:' => 'confusedsad.png',
	
		':dead:' => 'dead.png',
		':delicious:' => 'delicious.png',
		':depressed:' => 'depressed.png',
		':evil:' => 'evil.png',
		':evilgrin' => 'evilgrin.png',

		':grin:' => 'grin.png',
		':impatient:' => 'impatient.png',
		':inlove:' => 'inlove.png',
		':kiss:' => 'kiss.png',
		':mad:' => 'mad.png',
	
	
		':nerdy:' => 'nerdy.png',
		':notfunny:' => 'notfunny.png',
		'ohrly:' => 'ohrly.png',
		':reallyevil:' => 'reallyevil.png',
		':sarcasm:' => 'sarcasm.png',
	 

		':shocked:' => 'shocked.png',
		':sick:' => 'sick.png',
		':silly:' => 'silly.png',
		':sing:' => 'sing.png',
		':smitten:' => 'smitten.png',
	
		':smug:' => 'smug.png',
		':stress:' => 'stress.png',
		':sunglasses:' => 'sunglasses.png',
		':sunglasses2:' => 'sunglasses2.png',
		':superbashfulcute:' => 'superbashfulcute.png',
	
		':tired:' => 'tired.png',
		':whistle:' => 'whistle.png',
		':winktongue:' => 'winktongue.png',
		':yawn:' => 'yawn.png',
		':zipped:' => 'zipped.png',

	);
	
	
	$codes = array_keys($emoji['emoticons']);
	$images = array_values($emoji['emoticons']);
		
	$tmp = array ();
	foreach ( $images as $k => $v ) {
		$tmp [$k] = '<img class="usc_emoji" style="vertical-align: sub;" src="'.$_POST['applicationDir'].'/widget/emoji/img/' . $v . '" alt="emoticon" />';
	}
	
	return str_ireplace ( $codes, $tmp, $text );
}

echo json_encode($output);

die();
