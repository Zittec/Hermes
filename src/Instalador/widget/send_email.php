<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

date_default_timezone_set('UTC');

include("../data/config/config.php");
$PDO = PDO_CONNECT();

$command = "SELECT `email` FROM `users` WHERE `user_id` = '1'";
$result = $PDO->prepare($command);
$result->execute();

$return = array();
while ($d = $result->fetch(PDO::FETCH_ASSOC)) {
	$admin_email = $d['email'];
}

$_POST['contact_message'] = wordwrap($_POST['contact_message'], 70);
$encoded_data = json_decode(base64_decode($_POST['encodedGetVars']));
$init_info = "Mensaje fuera de línea mandado por: ".$_POST['contact_name']."\n\nPágina visitada: ".$encoded_data->current_url."\n\n\nMensaje:\n\n";

$to = $admin_email;
$subject = "[Hermes] Mensaje fuera de línea de ".$_POST['contact_name'];
$message =  $init_info.$_POST['contact_message'];
$headers = 'From: '.$_POST['contact_email']."\r\n" .'Reply-To: '.$_POST['contact_email']."\r\n".'X-Mailer: PHP/'.phpversion();

mail($to, $subject, $message, $headers);

die();