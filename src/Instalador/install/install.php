<?php

// GET APP DOMAIN & DIRECTORY //

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    //$domainName = $_SERVER['HTTP_HOST'] . '/';
    $domainName = $_SERVER['HTTP_HOST'] ;
    return $protocol . $domainName;
}

define('SITE_URL', siteURL());

$current_directory = $_SERVER['PHP_SELF'];
$curArr = explode('/', $current_directory);
unset($curArr[count($curArr) - 1]);
unset($curArr[count($curArr) - 1]);
$appDir = implode('/', $curArr);
$appPath = $_SERVER['DOCUMENT_ROOT'].$appDir;

$isInstalled = false;
if (file_exists($appPath . '/data/config/config.php')) {
    $isInstalled = true;
    header('Location: ' . siteURL() . $appDir . '/index.php');
    die();
}

if (isset($_POST['install']) && !$isInstalled) {
    $error = '';
	
} else {
    $_POST['mysql_host'] = '';
    $_POST['mysql_user'] = '';
    $_POST['mysql_pass'] = '';
    $_POST['mysql_db'] = '';
    $_POST['admin_username'] = '';
    $_POST['admin_password'] = '';
    $_POST['admin_mail'] = '';
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hermes - Instalación</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo SITE_URL . $appDir . '/data/css/bootstrap.min.css'; ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo SITE_URL . $appDir . '/install/css/install.min.css'; ?>" type="text/css">
    <!-- jQuery 2.1.4 -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

</head>
<body>

<header>
    <div class="header-content">
        <div class="header-content-inner">
            <h1>Bienvenid@ a Hermes</h1>
            <hr>
            <p>Para instalar el software necesitas configurar una conexión a base de datos. Por favor revisa las instrucciones de instalación antes de continuar.</p>
            <a onclick="startInstall()" class="btn btn-primary btn-xl">Instalar</a>
        </div>
    </div>
</header>


<script>

    (function ($) {

        $.fn.fitText = function (kompressor, options) {

            var compressor = kompressor || 1,
                settings = $.extend({
                    'minFontSize': Number.NEGATIVE_INFINITY,
                    'maxFontSize': Number.POSITIVE_INFINITY
                }, options);

            return this.each(function () {

                var $this = $(this);

                var resizer = function () {
                    $this.css('font-size', Math.max(Math.min($this.width() / (compressor * 10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
                };

                resizer();
                $(window).on('resize.fittext orientationchange.fittext', resizer);
            });

        };

    })(jQuery);

    $("h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '65px'
        }
    );


    function startInstall() {
        $(".header-content-inner").html('');
        $('.header-content').css({'top': '30px', '-webkit-transform': 'initial', 'transform': 'initial'});
        $('#content').contents().appendTo('.header-content-inner');
    }


</script>

<div id="content">

    <?php
    if ($isInstalled) {
        ?>

        <div class="box_error">
            <strong>¡Hermes ha sido instalado!</strong>
            <br/><br/>
            <strong>Por favor ELIMINA la carpeta de instalación: /install/</strong>
        </div>
        <?php
    } elseif (isset($complete)) {
        ?>
        <div class="box_complete">
            <strong>¡Instalación terminada!</strong>
            <br/><br/>
            <a href="<?php echo '/' . $appDir . '/users/login' ?>" title="">Acceder a Hermes</a>
        </div>
        <?php
    }
    else {
    ?>

    <?php
    if (isset($error) && $error != '') {
        ?>
        <div class="box_error"><?php echo $error; ?></div>

    <?php }

    $fas_error = false;

    if (@substr(sprintf('%o', fileperms($appPath . '/engine/data/')), -4) != '0777') {
        $errorArr[] = '<div class="box_error"><strong>Requerido: </strong>Por favor establece persmisos de escritura (0777) a la carpeta: <strong>/engine/data</strong>.</div><br/>';
        $fas_error = true;
    }
    if (@substr(sprintf('%o', fileperms($appPath . '/data/config/')), -4) != '0777') {
        $errorArr[] = '<div class="box_error"><strong>Requerido: </strong>Por favor establece persmisos de escritura (0777) a la carpeta: <strong>/data/config</strong>.</div><br/>';
        $fas_error = true;
    }

    if (!function_exists('fopen')) {

        $errorArr[] = '<div class="box_error"><strong>Requerido: </strong>La función "fopen" debe estar activada</div><br/>';
        $fas_error = true;
    }
    if (!function_exists('scandir')) {
        $errorArr[] = '<div class="box_error"><strong>Requerido: </strong>La función "scandir" debe estar activada</div><br/>';
        $fas_error = true;
    }
    if (!version_compare("5.2", phpversion(), "<=")) {
        $errorArr[] = '<div class="box_error"><strong>Requerido: </strong>PHP >= 5.2 es requerido</div><br/>';
        $fas_error = true;
    }
	
	if (isset($_GET['mode']) && $_GET['mode'] == 0){
		$fas_error = false;
	}
	
    if ($fas_error == true) {

        ?>

        <h1>Instalación de Hermes</h1>
        <br/>

        <div class="box box-info">
            <h3>Revisión de permisos</h3>
            <br/><br/>
        </div>

        <?php

        foreach ($errorArr as $e) {
            print_r($e);
            echo '<br/>';
        }
		
		$alert = "Are you sure?";
		echo '<small><a style="color:#000000" href="'.siteURL() . $appDir . '/install/install.php?mode=0" onclick=\'alert("¿Estás segur@? Por favor cersiórate de que las carpetas necesarias tienen permisos de lectura y escritura")\'>Ignorar y continuar</a></small>';
		
    }


    if (!$fas_error) {

    ?>
    <div>
        <div id="db_connection">
            <h1>Instalación de Hermes</h1>
            <br/>

            <div class="box box-info">
                <br/>

                <h3>Configuración de base de datos</h3>

                <div class="sk-cube-grid">
                    <div class="sk-cube sk-cube1"></div>
                    <div class="sk-cube sk-cube2"></div>
                    <div class="sk-cube sk-cube3"></div>
                    <div class="sk-cube sk-cube4"></div>
                    <div class="sk-cube sk-cube5"></div>
                    <div class="sk-cube sk-cube6"></div>
                    <div class="sk-cube sk-cube7"></div>
                    <div class="sk-cube sk-cube8"></div>
                    <div class="sk-cube sk-cube9"></div>
                </div>


                <div id="db_status" class="db_status_red">&nbsp;</div>

                <div class="box-body">
                    <!-- form start -->
                    <form class="form-horizontal" action="" method="post" id="database_connection_form"
                          autocomplete="off">

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Host de base de datos" id="db_host"
                                       name="db_host" value="localhost" required/>
                                <i class="pull-left">Este será localhost en la mayoría de los casos.</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Nombre de base de datos" id="db_name"
                                       name="db_name" value="" required/>
                                <i class="pull-left">Nombre de la base de datos</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <label class="input_null">
                                    <input class="input_null" type="text" name="null1"/>
                                    <input class="input_null" type="password" name="null2"/>
                                </label>
                                <input type="text" class="form-control" placeholder="Usuario" id="db_user"
                                       name="db_user" value="" autocomplete="off" required/>
                                <i class="pull-left">Usuario con acceso a la base de datos</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" placeholder="Contraseña" id="db_password"
                                       name="db_password" value="" autocomplete="off" required/>
                                <i class="pull-left">Contraseña del usuario</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>


                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-xl">Probar conexión</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box-body -->
            </div>

            <script>

                if (document.getElementById("database_connection_form") != undefined) {
                    document.getElementById("database_connection_form").onsubmit = function () {
                        $('.sk-cube-grid').css('visibility', 'visible');

                        var dbHost = encodeURIComponent($('#db_host').val());
                        var dbName = encodeURIComponent($('#db_name').val());
                        var dbUser = encodeURIComponent($('#db_user').val());
                        var dbPassword = encodeURIComponent($('#db_password').val());

                        var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function () {
                            if (xhttp.readyState == 4 && xhttp.status == 200) {
                                $('.sk-cube-grid').css('visibility', 'hidden');
                                if (xhttp.responseText == 'true') {
                                    $('#db_connection').hide();
                                    $('#setup_admin').show();

                                } else {
                                    $('#db_status').html('Conexión fallida. Por favor revisa las credenciales e intenta de nuevo.');
                                }
                            }
                        };
                        xhttp.open("POST", "process.php", true);
                        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhttp.send("db_host=" + dbHost + "&db_name=" + dbName + "&db_user=" + dbUser + "&db_password=" + dbPassword + "&check_db=true");

                        return false;
                    }
                }

            </script>
        </div>


        <div id="setup_admin">

            <h1>Instalación de Hermes</h1>
            <br/>

            <div class="box box-info">
                <br/>

                <div id="response" class="db_status_green">Prueba de conexión a base de datos exitosa.</div>
                <h3>Configuración de cuenta de administrador</h3>

                <div class="sk-cube-grid">
                    <div class="sk-cube sk-cube1"></div>
                    <div class="sk-cube sk-cube2"></div>
                    <div class="sk-cube sk-cube3"></div>
                    <div class="sk-cube sk-cube4"></div>
                    <div class="sk-cube sk-cube5"></div>
                    <div class="sk-cube sk-cube6"></div>
                    <div class="sk-cube sk-cube7"></div>
                    <div class="sk-cube sk-cube8"></div>
                    <div class="sk-cube sk-cube9"></div>
                </div>

                <div class="box-body">
                    <!-- form start -->
                    <form class="form-horizontal" action="" method="post" id="admin_setup_form" autocomplete="off">

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" placeholder="Correo electrónico" id="admin_email"
                                       name="admin_email" value="" required/>
                                <i class="pull-left">Correo electrónico</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <label class="input_null">
                                    <input class="input_null" type="text" name="null1"/>
                                    <input class="input_null" type="password" name="null2"/>
                                </label>
                                <input type="text" class="form-control" placeholder="Nombre de usuario" id="admin_name"
                                       name="admin_name" value="" autocomplete="off" required/>
                                <i class="pull-left">Usuario para acceso a Hermes</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" placeholder="Contraseña" id="admin_password"
                                       name="admin_password" value="" autocomplete="off" required/>
                                <i class="pull-left">Contraseña para acceder a Hermes</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-xl">Terminar</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box-body -->

            </div>

            <script>


                if (document.getElementById("admin_setup_form") != undefined) {
                    document.getElementById("admin_setup_form").onsubmit = function () {
                        $('.sk-cube-grid').css('visibility', 'visible');

                        var appPath = '<?php echo $appPath; ?>';
                        var adminEmail = encodeURIComponent($('#admin_email').val());
                        var adminName = encodeURIComponent($('#admin_name').val());
                        var adminPassword = encodeURIComponent($('#admin_password').val());
					
                        var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function () {
                            if (xhttp.readyState == 4 && xhttp.status == 200) {
                                if (xhttp.responseText == 'installed') {
                                    $('#setup_admin').hide();
                                    $('#install_complete').show();
                                }
								else{
									$('#response').html(xhttp.responseText).css('color', '#FF0000');
								}
                            }
                        };
                        xhttp.open("POST", "process.php", true);
                        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhttp.send("admin_email=" + adminEmail + "&admin_name=" + adminName + "&admin_password=" + adminPassword + "&app_path=" + appPath + "&admin_setup=true");

                        return false;
                    }
                }

            </script>
        </div>


        <div id="install_complete">
            <h1>Instalación de Hermes</h1>
            <br/>

            <h3>Instalación terminada</h3>

            <p><a onclick="location.reload();" class="pointer">Acceder a Hermes</a></p>

        </div>

        <?php } ?>

        <?php
        }
        ?>


    </div>
    <!-- end of content -->

</body>
</html>
